# Objective 

IMP Note: CORS has not been setup, so please manage to use ` chrome browser ` with ` cors plugin ` 
available [ CORS plugin available here ](https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi?utm_source=chrome-app-launcher-info-dialog)

    Please enable this extension, else you will get error.
    
    
Note: this is a test project, trying to incorporate " Frontend Framework Angular with Backend Laravel"

This is a test project that i have been doing in my extra time @ home, so things are not polished and shiny. 

Objective is to create crud operations fast and easy: 


# What is already done behind the scene: 

Angular

    - Basic Angular setup and below points are already setup
    - API integraatoin with Laravel 
    - Interceptors, Routing
    
Laravel: 

    - SQLite, Passport, API Setup and more is already set behind the scene


# What you get ?

    - Fast 
    - Easy
    - Automatic front end and matching backend validation within miunutes.. 
    - sleek Bootstrap 4 validation in no time.. 
    

# default users present in sqlite

```
user: first@first.com   password: first@first.com

user: second@second.com password: second@second.com

user: third@third.com   password: third@third.com
```    

If you have any question, please write me back to : myFirstName.myLastName {@} GMail