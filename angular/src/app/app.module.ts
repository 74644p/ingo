import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import {Routes, RouterModule} from '@angular/router';

import {AppComponent} from './app.component';

import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {AuthPsInterceptor} from './services/auth.ps.interceptor';
import {SignupComponent} from './ts/template/signup.component';


import {MySubRtModule} from './ps-routes/ps-routes.module';
import {NavComponent} from './ts/template/nav.component';
import {FooterComponent} from './ts/template/footer.component';
import {LoginComponent} from './ts/login.component';
import {LogoutComponent} from './ts/logout.component';
import {PsSrvService} from './services/ps-srv.service';
import { ProfileComponent } from './ts/profile.component';
import { WebLogComponent } from './ts/web-log.component';
import { WebLogListComponent } from './ts/web-log-list.component';
import { WebLogFormComponent } from './ts/web-log-form.component';

const appRoutes: Routes = [
    // {path: 'signup', component: SignupComponent},
];

@NgModule({
    declarations: [
        AppComponent,
        SignupComponent,
        NavComponent,
        FooterComponent,
        LoginComponent,
        LogoutComponent,
        ProfileComponent,
        WebLogComponent,
        WebLogListComponent,
        WebLogFormComponent,
    ],
    imports: [
        BrowserModule, ReactiveFormsModule, HttpClientModule,
        RouterModule.forRoot(appRoutes),
        MySubRtModule,
    ],
    providers: [
        PsSrvService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthPsInterceptor,
            multi: true
        }

    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
