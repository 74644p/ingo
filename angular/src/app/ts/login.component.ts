import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {PsSrvService} from '../services/ps-srv.service';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: '../views/login.component.html',
    styleUrls: ['../views/css/login.component.css'],
})
export class LoginComponent implements OnInit {

    constructor(private psServices: PsSrvService, private http: HttpClient, private router: Router) {
    }

    login: FormGroup;
    results: any;
    postUrl: string = this.psServices.psApiURI + '/api/login'; // change your end point here not URI

    ngOnInit() {
        this.login = new FormGroup({
            'email': new FormControl(),
            'password': new FormControl(),
        });
        this.psServices.PS_isLoggedIn();
    }

    onLogin() {
        if (this.psServices.cl(this.login)) {
            console.log(this.login.value);

            // Make the HTTP request:
            this.http.post(this.postUrl, this.login.value).subscribe(data => {
                    // Read the result field from the JSON response.
                    this.results = data;
                },
                (err) => {  // The 2nd callback handles errors
                    console.log('your error starts');
                    console.error(err);
                },
                () => { // The 3rd callback handles the "complete" event.
                    console.log('your after complete action here');
                    console.log(this.results);
                    // console.log(this.results.success.token);
                    localStorage.setItem('id_token', this.results.success.token);
                    this.psServices.psIsValidUser = true;
                    this.psServices.PS_isLoggedIn();

                    this.router.navigate(['profile']);
                }
            );

        }
        else {
            console.log('Form invalid');
        }
    }


}
