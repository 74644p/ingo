 // start  

import { Component, OnInit } from '@angular/core';

import { PsSrvService } from './../services/ps-srv.service';
import {HttpClient} from '@angular/common/http';


@Component({
selector: 'app-web-log-list',
templateUrl: '../views/web-log-list.component.html',
providers: [PsSrvService]

})
export class WebLogListComponent implements OnInit {

    results: Object;
    preEndPointUri: string = this.psServices.psApiURI + '/api/web-log/'; // change your end point here not URI
    loaderAnimation: boolean = true;

    data: any = null;
    record_total: number;
    records_per_page: number;
    current_page: number;
    last_page: number;
    records_from: number;
    records_to: number;
    id: number;
    num_array: any;

    constructor(private http: HttpClient, private psServices: PsSrvService) {
    }

    ngOnInit() {
        const full_path = window.location.pathname.split('/');
        this.id = parseInt(full_path[3]);
        this.getData(this.id);
    }

    onPaginate(value = 0) {
        this.getData(value);
    }


    deleteRecord(id = 0) {
        console.log('deleting ' + id);

        this.http.post(this.preEndPointUri + 'delete/' + id, '').subscribe(api_data => {
        // Read the result field from the JSON response.
        this.results = api_data;
        },
        (err) => {  // The 2nd callback handles errors
        console.log('your error starts');
        console.error(err);
        },
        () => { // The 3rd callback handles the "complete" event.
        console.log('your after complete action here');
        console.log(this.results);
        document.getElementById('web-log-list-' + id).style.textDecoration = 'line-through';
        document.getElementById('web-log-list-' + id).style.color = 'pink';
        document.getElementById('web-log-list-' + id + '-links').remove();
        });
    }



    getData(value = 0) {
            console.log('PENDING: pulling data for clicked value = ' + value);
            this.loaderAnimation = true;
            this.http.post(this.preEndPointUri + '?page=' + value, '').subscribe(api_data => {
            // Read the result field from the JSON response.
            this.results = api_data;
            this.data = api_data['data']['data'];
            this.current_page = api_data['data']['current_page'];
            this.last_page = api_data['data']['last_page'];
            this.record_total = api_data['data']['total'];
            this.records_per_page = api_data['data']['per_page'];
            this.records_from = api_data['data']['from'];
            this.records_to = api_data['data']['to'];
            this.num_array = Array.from(Array(this.last_page).keys());

        },
            (err) => {  // The 2nd callback handles errors
            console.log('your error starts');
            console.error(err);

        },
        () => { // The 3rd callback handles the "complete" event.
            console.log('your after complete action here');
            console.log(this.results);

            // document.getElementById('login_form').remove();
            this.loaderAnimation = false;
            setTimeout(function () {
            var removeFront = document.getElementById('web-log-list-front');
            if (removeFront) removeFront.remove();
            document.getElementById('web-log-list').classList.add('flipped');

            }, 100);
            // console.log(document.getElementById('flip_id')) ; // .classList; // .toggle('flipped');
        });
    }



}


 // end 