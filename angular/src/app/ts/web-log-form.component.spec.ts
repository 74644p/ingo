import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebLogFormComponent } from './web-log-form.component';

describe('WebLogFormComponent', () => {
  let component: WebLogFormComponent;
  let fixture: ComponentFixture<WebLogFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebLogFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebLogFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
