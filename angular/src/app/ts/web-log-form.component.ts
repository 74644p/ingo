 // start  

import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PsSrvService } from './../services/ps-srv.service';

import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';


@Component({
selector: 'app-web-log-form',
templateUrl: '../views/web-log-form.component.html',
providers: [PsSrvService]

})
export class WebLogFormComponent implements OnInit {

    constructor(private http: HttpClient, private psServices: PsSrvService, private router: Router) {
    }

    WebLogForm: FormGroup  ;
    results: Object;

    preEndPointUri : string = this.psServices.psApiURI + '/api/web-log/'; // change your end point here not URI

    id: number = 0;
    buttonLabel: string = ' Create New Record ';
    isUpdate: boolean = false;

    loaderAnimation: boolean = true;
    formComplete: boolean = false;



    public WebLogArray : any = { "name" : "", "email" : "", "phone" : "", "detail" : "", "is_public" : "",  }; // for update replace this with  your object

    ngOnInit() {
        const fullPath = window.location.pathname;
        const full_pathArr = fullPath.split('/');

        this.id = parseInt(full_pathArr[3]);

        if (fullPath != '/web-log/add') {
            this.buttonLabel = "Update Record";
            this.isUpdate = true;
            console.log('isUpdate : ' + this.isUpdate);
            console.log('getting record');

            this.http.post(this.preEndPointUri + 'get/' + this.id, 'test').subscribe(api_data => {
                // Read the result field from the JSON response.
                this.results = api_data;
                this.WebLogForm.patchValue(this.results['data']);
                },
                (err) => {  // The 2nd callback handles errors
                    console.log('redirect logic');
                    console.error(err);
                    this.router.navigate(['web-log/page/1']);
                    alert('an error occurred. redirecting to list page');

                },
                () => { // The 3rd callback handles the "complete" event.
                    console.log('your after complete action here');
                    console.log(this.results);
                    this.loaderAnimation = false;
                }
            );
        }
        else {
            this.loaderAnimation = false;
        }


    this.WebLogForm = new FormGroup({
    // loop your stuff here
                    'name' : new FormControl(this.WebLogArray.name, [  Validators.required ,  Validators.minLength(3) ,  ] ),
                    'email' : new FormControl(this.WebLogArray.email, [  Validators.required ,  Validators.minLength(3) ,  Validators.email ,  ] ),
                    'phone' : new FormControl(this.WebLogArray.phone, [  ] ),
                    'detail' : new FormControl(this.WebLogArray.detail, [  Validators.required ,  Validators.minLength(5) ,  ] ),
                    'is_public' : new FormControl(this.WebLogArray.is_public, [  ] ),
    
        });
    }

    onSubmit(id: number = 0, formSubmitUpdate: boolean = false) {
        if (this.psServices.cl(this.WebLogForm)) {
            console.log(" \n\n\n do you business logic here : Below is your posted JSON data \n \n ");
            console.log(this.WebLogForm.value);

            // Make the HTTP request:
            var endPointUrl = this.preEndPointUri + 'put/';
            if (formSubmitUpdate) {
            endPointUrl = this.preEndPointUri + 'patch/' + id;
            }

            this.http.post(endPointUrl, this.WebLogForm.value).subscribe(api_data => {
                    // Read the result field from the JSON response.
                    this.results = api_data;
                },
                (err) => {  // The 2nd callback handles errors
                    console.log('your error starts');
                    console.error(err);
                    this.router.navigate(['web-log/page/1']);
                },
                () => { // The 3rd callback handles the "complete" event.
                    console.log('your after complete action here');
                    console.log(this.results);
                    this.formComplete = true;
                }
            );

        }
        else {
            console.log('Form invalid');
        }
    }



}


 // end 