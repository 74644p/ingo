 // start  

import { Component, OnInit } from '@angular/core';
import { PsSrvService } from './../services/ps-srv.service';

import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';


@Component({
selector: 'app-web-log',
templateUrl: '../views/web-log.component.html',
providers: [PsSrvService]

})
export class WebLogComponent implements OnInit {

    constructor(private http: HttpClient, private psServices: PsSrvService, private  router: Router) {
    }

    ngOnInit() {
        if(window.location.pathname == '/web-log') {
            console.log('redirect logic here');
            this.router.navigate(['web-log/page/1']);
        }
    }

}

 // end 