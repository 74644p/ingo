import {Component, OnInit} from '@angular/core';

import {FormGroup, FormControl, Validators} from '@angular/forms';
import {PsSrvService} from '../../services/ps-srv.service';

import {HttpClient} from '@angular/common/http';
import {Router} from "@angular/router";


@Component({
    selector: 'app-signup',
    templateUrl: '../../views/signup.component.html',
    styleUrls: ['../../views/css/signup.component.css'],
    providers: [PsSrvService]

})
export class SignupComponent implements OnInit {

    constructor(private http: HttpClient, private psServices: PsSrvService, private router: Router) {
    }

    results: any; // changed from object to any
    signupForm: FormGroup;
    postUrl: string = this.psServices.psApiURI + '/api/register'; // change your end point here not URI


    public signupArray: any = {"name": "", "email": "", "password": "", "c_password": "",}; // for update replace with this your object


    ngOnInit() {
        this.signupForm = new FormGroup({
// loop your stuff here
            'name': new FormControl(this.signupArray.name, []),
            'email': new FormControl(this.signupArray.email, []),
            'password': new FormControl(this.signupArray.password, []),
            'c_password': new FormControl(this.signupArray.c_password, []),

        });
    }

    onSubmit() {
        if (this.psServices.cl(this.signupForm)) {
            console.log(" \n\n\n do you business logic here : Below is your posted JSON data \n \n ");
            console.log(this.signupForm.value);

            // Make the HTTP request:
            this.http.post(this.postUrl, this.signupForm.value).subscribe(data => {
                    // Read the result field from the JSON response.
                    this.results = data;
                },
                (err) => {  // The 2nd callback handles errors
                    console.log('your error starts');
                    console.error(err);
                },
                () => { // The 3rd callback handles the "complete" event.
                    console.log('your after complete action here');
                    console.log(this.results);

                    // setting local storage
                    console.log('setting local storage via token');
                    localStorage.setItem('id_token', this.results.success.token);

                    this.psServices.psIsValidUser = true;
                    this.psServices.PS_isLoggedIn();

                    this.router.navigate(['/']);
                    this.router.navigate(['profile']);





                }
            );

        }
        else {
            console.log('Form invalid');
        }
    }

}


