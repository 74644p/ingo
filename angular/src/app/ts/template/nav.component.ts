import {Component, OnInit} from '@angular/core';
import {PsSrvService} from '../../services/ps-srv.service';

@Component({
    selector: 'app-nav',
    templateUrl: '../../views/nav.component.html',
    styleUrls: ['../../views/css/nav.component.css'],
    // providers: [PsSrvService]
})
export class NavComponent implements OnInit {

    dynIsUser = false;

    constructor(private  psServices: PsSrvService) {
    }


    ngOnInit() {
        console.log(this.psServices.psIsValidUser);


    }

    showNav() {
        if (document.getElementById('nav').classList.contains('collapse')) document.getElementById('nav').classList.remove('collapse');
        else this.hideNav();
    }

    hideNav() {
        document.getElementById('nav').classList.add('collapse');
    }

}
