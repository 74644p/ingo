import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebLogListComponent } from './web-log-list.component';

describe('WebLogListComponent', () => {
  let component: WebLogListComponent;
  let fixture: ComponentFixture<WebLogListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebLogListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebLogListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
