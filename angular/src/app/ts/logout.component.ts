import {Component, OnInit} from '@angular/core';
import {PsSrvService} from '../services/ps-srv.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-logout',
    templateUrl: '../views/logout.component.html',
    styleUrls: ['../views/css/logout.component.css'],
})
export class LogoutComponent implements OnInit {

    constructor(public  psServices: PsSrvService, private router: Router) {
    }

    ngOnInit() {
        localStorage.removeItem('id_token');
        this.psServices.PS_isLoggedIn();
        this.router.navigate(['/'] , { queryParams: { logout: true } });
    }
}
