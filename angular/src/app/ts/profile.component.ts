import {Component, OnInit} from '@angular/core';
import {PsSrvService} from '../services/ps-srv.service';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';

@Component({
    selector: 'app-profile',
    templateUrl: '../views/profile.component.html',
    styleUrls: ['../views/css/profile.component.css']
})
export class ProfileComponent implements OnInit {

    constructor(private psServices: PsSrvService, private  router: Router, private http: HttpClient) {
    }
    results: any;
    postUrl: string = this.psServices.psApiURI + '/api/get-details'; // change your end point here not URI
    ngOnInit() {
        if (!this.psServices.psIsValidUser) {
            // console.log('not logged in ');
            this.router.navigate(['login'], {queryParams: {m: 'auth_required'}});
        }else {
            console.log('pulling user details');

            // Make the HTTP request:
            this.http.post(this.postUrl, '').subscribe(data => {
                    // Read the result field from the JSON response.
                    this.results = data;
                },
                (err) => {  // The 2nd callback handles errors
                    console.log('your error starts');
                    console.error(err);
                },
                () => { // The 3rd callback handles the "complete" event.
                    console.log('your after complete action here');
                    console.log(this.results);

                }
            );

        }
    }

}
