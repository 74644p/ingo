import {Injectable} from '@angular/core';
import {Router} from '@angular/router';

@Injectable()
export class PsSrvService {

    constructor( private router: Router ) {
    }

    psApiURI = 'http://localhost:8000';
    psIsValidUser = (localStorage.getItem('id_token') !== null);

    cl(formObject: any) {

        if (formObject.valid) {
            this.PS_isLoggedIn(); // let this run every time
            return true;
        }
        else {
            const eachFields = formObject.controls;
            for (const id in eachFields) {
                if (!formObject.controls[id].valid) {
                    document.getElementById(id).focus();
                    document.getElementById(id).blur();
                    document.getElementById(id).focus();
                    break;
                }
            }
            this.PS_isLoggedIn(); // let this run every time
            return false;
        }
    }

    PS_isLoggedIn() {
        if (localStorage.getItem('id_token') === null ) {
            console.log('not logged in ');
            this.psIsValidUser = false;
        }
        else {
            this.psIsValidUser = true;
            console.log('logged in.. check validity of token: PENDING ...');
        }
        console.log('psIsValidUser:' + this.psIsValidUser);
    }


    PS_guard() {
        if (!this.psIsValidUser) {
            console.log('PS_guard in action.. ');
            this.router.navigate(['/login']);
        }
    }
}
