import { TestBed, inject } from '@angular/core/testing';

import { PsSrvService } from './ps-srv.service';

describe('PsSrvService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PsSrvService]
    });
  });

  it('should be created', inject([PsSrvService], (service: PsSrvService) => {
    expect(service).toBeTruthy();
  }));
});
