import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {SignupComponent} from '../ts/template/signup.component';
import {LoginComponent} from '../ts/login.component';
import {LogoutComponent} from '../ts/logout.component';
import {ProfileComponent} from '../ts/profile.component';
import {WebLogComponent} from "../ts/web-log.component";
import {WebLogListComponent} from "../ts/web-log-list.component";
import {WebLogFormComponent} from "../ts/web-log-form.component";

const routes: Routes = [
    {path: 'signup', component: SignupComponent},
    {path: 'sub', component: SignupComponent},
    {path: 'login', component: LoginComponent},
    {path: 'logout', component: LogoutComponent},
    {path: 'profile', component: ProfileComponent},


    {   // Make sure to import each component @ top
        path: 'web-log', component: WebLogComponent,
        children: [
            {path: 'page/:id', component: WebLogListComponent},
            {path: 'add', component: WebLogFormComponent},
            {path: 'get/:id', component: WebLogFormComponent},
            {path: 'edit/:id', component: WebLogFormComponent},
            {path: 'delete/:id', component: WebLogComponent},
        ]
    },


];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PsRoutesRoutingModule {
}
