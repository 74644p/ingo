import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PsRoutesRoutingModule } from './ps-routes-routing.module';

@NgModule({
  imports: [
    CommonModule,
    PsRoutesRoutingModule
  ],
  declarations: []
})
export class MySubRtModule { }
