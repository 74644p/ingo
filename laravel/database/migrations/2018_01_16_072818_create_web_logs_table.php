<?php   
    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateWebLogsTable extends Migration

    {
    public function up()
    {
    Schema::create('web_logs', function (Blueprint $table) {
        $table->increments('id');

        $table->char('name')->nullable() ;
        $table->char('email')->nullable() ;
        $table->char('phone')->nullable() ;
        $table->text('detail')->nullable() ;
        $table->boolean('is_public')->nullable() ;

        $table->softDeletes();
        $table->unsignedInteger('user_id');
        $table->timestamps();

        });
    }

    public function down()
    {
    Schema::dropIfExists('web_logs');
    }
}

 // end 