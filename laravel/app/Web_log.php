<?php   namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Web_log extends Model
    {
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = [ 'sys_main_tbl', 'sys_main_tbl_plural', 'name', 'email', 'phone', 'detail', 'is_public',  'user_id' ]  ; //  fillable stuff here
    }


 // end 