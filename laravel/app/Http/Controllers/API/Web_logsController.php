<?php   namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

use App\Web_log;

use App\Http\Controllers\Controller;
use Validator;


class Web_logsController extends Controller
    {
    public function __construct()
    {
    $this->middleware('auth', [ 'except' => ['detail'] ] ); // require auth except for detail view
    // $this->middleware('auth'); // require auth for every method
    }

    public function PS_returnJSON($status = 'status', $code = 404, $message = 'Message', $data = [])
    {
    return response()->json(['status' => $status, 'code' => $code, 'message' => $message, 'data' => $data]);
    }



    public function PS_getResultOrX($id, $skipAuth = false)
    {
    header('Content-Type: application/json');
    $web_log = Web_log::find($id);
    if ($web_log == null) {
    // do your event logging here
    exit($this->PS_returnJSON('error', 404, 'Record does not exist', 'none'));
    }
    if ($skipAuth == false) {
    if (Auth::id() != $web_log->user_id) {
        // do your event logging here
         exit($this->PS_returnJSON('error', 403, 'Forbidden', 'none'));
        }
    }

    return $web_log;
    }



    public function master_list(){ // master list
    $web_logs = Web_log::where('user_id', Auth::id() )
    ->orderBy('id', 'desc')->paginate(3);// change your number here

    $web_logs = $web_logs->appends(Input::except('page'));
    return response()->json(['status' => 'Success', 'code' => 200, 'data' => $web_logs]);
    }


    public function get(Web_log $web_log, $id) // detail
    {
    $web_log = $this->PS_getResultOrX($id, true); // true skips the author check
    return $this->PS_returnJSON('select', 200, 'select success', $web_log);
    }




    public function upsert(Web_log $web_log, $id = null , Request $request )
    {


    $PS_validator = Validator::make($request->all(),
        [
	'name' => 'required|min:3', 
	'email' => 'required|min:3|email', 
	'detail' => 'required|min:5', 
        ]
    );
    if ($PS_validator->fails()) return $this->PS_returnJSON('error', 409, 'validation error', $PS_validator->errors());


    try {

    if (($id === null)) { // if insert
    $ins = Web_log::create(['user_id' => Auth::id(), 'name' => request('name'), 'email' => request('email'), 'phone' => request('phone'), 'detail' => request('detail'), 'is_public' => request('is_public'),  ]);
    return $this->PS_returnJSON('insert', 200, 'insert success', $ins);
    }


    $web_log = $this->PS_getResultOrX($id); // user check

    $web_log_update = Web_log::find($id);
    // dynamic generation
       $web_log_update->name = request('name'); 
       $web_log_update->email = request('email'); 
       $web_log_update->phone = request('phone'); 
       $web_log_update->detail = request('detail'); 
       $web_log_update->is_public = request('is_public'); 

    $web_log_update->save();
    }
    catch (\Exception $e) {
    $mySQL_err = 'MySQL Error Code:' . $e->getCode() . ' - ';   // err code
    return $this->PS_returnJSON('error', 500, $mySQL_err, 'none');
    exit;

    } // try catch ends
    return $this->PS_returnJSON('updated', 200, 'update success', $web_log_update);
    }

    public function delete(Web_log $web_log, $id)
    {
    $web_log = $this->PS_getResultOrX($id );
    $web_log_destroy_id = Web_log::destroy($id);
    return $this->PS_returnJSON('destroy', 200, 'below record destroyed = ' . $web_log_destroy_id, $web_log);
    } // class close

}


 // end 